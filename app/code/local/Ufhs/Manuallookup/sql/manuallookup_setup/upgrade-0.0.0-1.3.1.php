<?php
$installer = $this;
$installer->startSetup();
$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('manuallookup/videos')};
	CREATE TABLE {$installer->getTable('manuallookup/videos')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`prod_id` int(11) unsigned NOT NULL,
	`url` varchar(255) NOT NULL,
	`name` varchar(255) NOT NULL default '',
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");
$installer->endSetup();
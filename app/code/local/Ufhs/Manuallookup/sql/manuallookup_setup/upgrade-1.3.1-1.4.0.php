<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('manuallookup/resources'),'store',array(
	'type' => 'integer',
	'nullable' => false,
	'length' => 10,
	'default' => 0,
	'comment' => 'Store View'
));

$installer->getConnection()
->addColumn($installer->getTable('manuallookup/videos'),'store',array(
	'type' => 'integer',
	'nullable' => false,
	'length' => 10,
	'default' => 0,
	'comment' => 'Store View'
));

$installer->endSetup();
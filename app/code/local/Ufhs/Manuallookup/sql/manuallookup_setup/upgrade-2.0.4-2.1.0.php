<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('manuallookup/videos'),'disabled',array(
	'type' => 'boolean',
	'nullable' => false,
	'length' => 1,
	'default' => 0,
	'comment' => 'Disabled'
));

$installer->endSetup();
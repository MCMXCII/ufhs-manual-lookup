<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('manuallookup/resources'),'order',array(
	'type' => 'integer',
	'nullable' => false,
	'length' => 10,
	'default' => 0,
	'comment' => 'Priority'
));

$installer->getConnection()
->addColumn($installer->getTable('manuallookup/videos'),'order',array(
	'type' => 'integer',
	'nullable' => false,
	'length' => 10,
	'default' => 0,
	'comment' => 'Priority'
));

$installer->endSetup();
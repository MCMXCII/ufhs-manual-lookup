<?php
class Ufhs_Manuallookup_Model_Resource_Resourcestype_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('manuallookup/resourcestype');
	}
}
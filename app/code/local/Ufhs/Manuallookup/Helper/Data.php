<?php

class Ufhs_Manuallookup_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static function checkVideo($code)
	{
		$url = 'https://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=' . $code . '&format=json';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_exec($curl);
		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		return $httpCode < 400 ? true : false;
	}

	public static function getVideoTitle($code)
	{
		$url = 'https://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=' . $code . '&format=json';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($curl);
		curl_close($curl);

		$result = json_decode($json);

		if ($result) {
			return $result->title;
		} else {
			return 'Video Title';
		}
	}
}
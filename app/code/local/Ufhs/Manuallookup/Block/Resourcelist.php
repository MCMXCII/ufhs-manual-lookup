<?php

class Ufhs_Manuallookup_Block_Resourcelist extends Mage_Core_Block_Template
{
	/**
	* User Interface List Block
	*
	* @package Manual Lookup
	* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
	*/

	private function _getIds()
	{
		$ids = [];
		$collection = Mage::getModel('manuallookup/resources')->getCollection()->addFieldToSelect('prod_id');
		foreach ($collection as $item) {
			$ids[$item['prod_id']] = $item['prod_id'];
		}
		return $ids;
	}

	public function getBrands()
	{
		$ids = $this->_getIds();

		$return = [];
		$collection = Mage::getModel('catalog/product')
		->getCollection()
		->addFieldtoFilter('entity_id', $ids)
		->addAttributeToSelect('brand');
		foreach ($collection as $item) {
			if ($item['brand']) {
				$return[$item['brand']] = $item['brand_value'];
			}
		}
		asort($return);
		return $return;
	}

	public function getCats()
	{
		// Build a master array of all cateogrys names indexed by entity_id
		$catName = [];
		$tmpCat = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('name');
		foreach ($tmpCat as $cat) {
			$catName[$cat['entity_id']] = $cat['name'];
		}

		// Get an array of all categories that contain a product with a resource
		$ids = $this->_getIds();
		$cats = [];
		$collection = Mage::getModel('catalog/product')
		->getCollection()
		->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
		->addFieldtoFilter('entity_id', $ids)
		->addAttributeToSelect('category_id');
		foreach ($collection->getData() as $item) {
			if (!isset($cats[$item['category_id']])) {
				$cats[$item['category_id']] = $item['category_id'];
			}
		}

		// Using the category array produce a nested array with the parent category as group index
		$return = [];
		$collection = Mage::getModel('catalog/category')
		->getCollection()
		->addFieldtoFilter('entity_id', $cats)
		->addAttributeToSelect('parent_id');
		foreach ($collection as $item) {
			$parentId = $item['parent_id'];
			$entityId = $item['entity_id'];
			if ($entityId) {
				if (!isset($return[$catName[$parentId]])) {
					$return[$catName[$parentId]] = [];
				}
				$return[$catName[$parentId]][$entityId] = $catName[$entityId];
			}
		}
		return $return;
	}
}
<?php
class Ufhs_Manuallookup_Block_Adminhtml_Renderers_Resourcepriority extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$video = $row->getResourceTypeId() === null  ? true : false;
		$value =  $row->getOrder();
		$id = $row->getId();
		$name = $video ? 'manuallookupVideoOrder' : 'manuallookupResourceOrder';
		return '<input style="width:50px;" type="number" name="' . $name . '[' . $id . ']" value="' . $value . '"/>';
	}
}
<?php
class Ufhs_Manuallookup_Block_Adminhtml_Renderers_Disabled extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		return $value ? 'Disabled' : 'Enabled';
	}
}
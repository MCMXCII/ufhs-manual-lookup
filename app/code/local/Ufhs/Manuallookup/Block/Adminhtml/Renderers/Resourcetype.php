<?php
class Ufhs_Manuallookup_Block_Adminhtml_Renderers_Resourcetype extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		$product = Mage::getModel('manuallookup/resourcestype')->load($value);
		return $product->getType();
	}
}
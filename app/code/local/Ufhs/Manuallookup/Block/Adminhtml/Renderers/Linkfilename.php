<?php
class Ufhs_Manuallookup_Block_Adminhtml_Renderers_Linkfilename extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		return '<a href="/media/ufhs/manuals/' . $value . '" target="_blank">' . $value . "</a>";
	}
}
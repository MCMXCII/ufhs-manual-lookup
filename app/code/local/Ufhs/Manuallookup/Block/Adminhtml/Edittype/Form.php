<?php
class Ufhs_Manuallookup_Block_Adminhtml_Edittype_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Resource');
		$this->setTemplate('manuallookup/admin/container-form.phtml');
	}

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'edittype',
			'action' => $this->getUrl('*/*/addtype', array('id' => $this->getRequest()->getParam('id'))),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
			)
		);
		$form->setUseContainer(true);
		$this->setForm($form);
		$form->addField('comment', 'text', array(
			'label' => Mage::helper('manuallookup')->__('Type'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'type',
			'value' => Mage::getModel('manuallookup/resourcestype')->load($this->getRequest()->getParam('id'))->getType()
			));

		return parent::_prepareForm();
	}
}
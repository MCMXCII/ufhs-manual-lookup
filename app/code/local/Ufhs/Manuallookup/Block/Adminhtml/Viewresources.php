<?php
class Ufhs_Manuallookup_Block_Adminhtml_Viewresources extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_viewresources';
		$this->_blockGroup = 'manuallookup';
		$this->_headerText = Mage::helper('manuallookup')->__('Resources');
		parent::__construct();
		$this->_updateButton('add', 'onclick',"setLocation('" . $this->getUrl('*/catalog_product') . "')");
	}
}
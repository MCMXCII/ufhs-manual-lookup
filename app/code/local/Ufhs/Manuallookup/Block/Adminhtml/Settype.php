<?php
class Ufhs_Manuallookup_Block_Adminhtml_Settype extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_settype';
		$this->_blockGroup = 'manuallookup';
		$this->_headerText = Mage::helper('manuallookup')->__('Resource Types');
		parent::__construct();
		$this->_updateButton('add', 'onclick',"setLocation('" . $this->getUrl('*/*/newtype') . "')");
	}
}
<?php
class Ufhs_Manuallookup_Block_Adminhtml_Allproductresources extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('manuallookupSettypeGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$productId = Mage::registry('current_product')->getId();
		$collection = Mage::getModel('manuallookup/resources')->getCollection()->addFieldToFilter('prod_id',$productId);
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('manuallookuptype', array(
			'header' => Mage::helper('manuallookup')->__('Type'),
			'align' => 'left',
			'width' => '100px',
			'index' => 'resource_type_id',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Resourcetype',
			'filter' => 'adminhtml/widget_grid_column_filter_select',
			'sortable' => true,
			'options' => $this->_getResourceTypes()
		));
		$this->addColumn('manuallookupname', array(
			'header' => Mage::helper('manuallookup')->__('Name'),
			'align' => 'left',
			'width' => '1000px',
			'index' => 'name'
		));
		$this->addColumn('manuallookupfilename', array(
			'header' => Mage::helper('manuallookup')->__('Filename'),
			'align' => 'left',
			'width' => '1000px',
			'index' => 'filename',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Linkfilename'
		));
		$this->addColumn('manuallookupstore', array(
			'header' => Mage::helper('manuallookup')->__('Store View'),
			'width' => '100px',
			'align' => 'left',
			'index' => 'store',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Storename'
		));
		$this->addColumn('manuallookuppriority', array(
			'header' => Mage::helper('manuallookup')->__('Priority'),
			'width' => '50px',
			'align' => 'left',
			'index' => 'order',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Resourcepriority'
		));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("manuallookup_block_adminhtml_settype_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getEmptyText()
	{
		return $this->__('There are no resources assigned to this product yet.');
	}

	public function getRowUrl($row)
	{
		return false;
	}

	private function _getResourceTypes()
	{
		$productId = Mage::registry('current_product')->getId();
		$collection = Mage::getModel('manuallookup/resources')->getCollection()->addFieldToFilter('prod_id',$productId);
		$returnArray = [];
		foreach($collection as $item)
		{
			$returnArray[$item->getResourceTypeId()] = Mage::getModel('manuallookup/resourcestype')->load($item->getResourceTypeId())->getType();
		}
		return $returnArray;
	}

	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('ids');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'    => Mage::helper('manuallookup')->__('Delete'),
			'url'      => $this->getUrl('*/manuallookup/massresourcedelete', ['prodid' => Mage::app()->getRequest()->getParam('id')])
		));

		return $this;
	}
}
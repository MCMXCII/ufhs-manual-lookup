<?php
class Ufhs_Manuallookup_Block_Adminhtml_Allproductvideos extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('manuallookupSetvideoGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$productId = Mage::registry('current_product')->getId();
		$collection = Mage::getModel('manuallookup/videos')->getCollection()->addFieldToFilter('prod_id',$productId);
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('manuallookupname', array(
			'header' => Mage::helper('manuallookup')->__('Name'),
			'align' => 'left',
			'width' => '1000px',
			'index' => 'name'
		));
		$this->addColumn('manuallookupurl', array(
			'header' => Mage::helper('manuallookup')->__('YouTube ID'),
			'align' => 'left',
			'width' => '100px',
			'index' => 'url',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Linkyoutube'
		));
		$this->addColumn('manuallookupstore', array(
			'header' => Mage::helper('manuallookup')->__('Store View'),
			'width' => '100px',
			'align' => 'left',
			'index' => 'store',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Storename'
		));
		$this->addColumn('manuallookupdisabled', array(
			'header' => Mage::helper('manuallookup')->__('Visible'),
			'width' => '100px',
			'align' => 'left',
			'index' => 'disabled',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Disabled'
		));
		$this->addColumn('manuallookuppriority', array(
			'header' => Mage::helper('manuallookup')->__('Priority'),
			'width' => '50px',
			'align' => 'left',
			'index' => 'order',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Resourcepriority'
		));

		return parent::_prepareColumns();
	}

	public function getEmptyText()
	{
		return $this->__('There are no videos associated with this product yet.');
	}

	public function getRowUrl($row)
	{
		return false;
	}
	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('ids');

		$this->getMassactionBlock()->addItem('enable', array(
			'label'    => Mage::helper('manuallookup')->__('Enable'),
			'url'      => $this->getUrl('*/manuallookup/massvideoenable', ['prodid' => Mage::app()->getRequest()->getParam('id')])
		));
		$this->getMassactionBlock()->addItem('disable', array(
			'label'    => Mage::helper('manuallookup')->__('Disable'),
			'url'      => $this->getUrl('*/manuallookup/massvideodisable', ['prodid' => Mage::app()->getRequest()->getParam('id')])
		));
		$this->getMassactionBlock()->addItem('delete', array(
			'label'    => Mage::helper('manuallookup')->__('Delete'),
			'url'      => $this->getUrl('*/manuallookup/massvideodelete', ['prodid' => Mage::app()->getRequest()->getParam('id')])
		));

		return $this;
	}
}
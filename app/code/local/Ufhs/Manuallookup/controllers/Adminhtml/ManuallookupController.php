<?php

class Ufhs_Manuallookup_Adminhtml_ManuallookupController extends Mage_Adminhtml_Controller_action
{
	/**
	* Main Adminhtml Controller
	*
	* @package Manual Lookup
	* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
	*/


	/**
	 * Layout Functions
	 * ----------------
	 * The layout functions provide a means to render a Magento adminhtml page,
	 * which we can then output blocks on using the layout.xml.
	 *
	 * The majority of the functionality has been refactord into the _outputLayout
	 * function, which only needs to take the title of the page as a parameter.
	 */

	protected function _initAction()
	{
		$this->_title($this->__('Manual Lookup'));
		$this->loadLayout();
		$this->_initLayoutMessages('adminhtml/session');
		$this->_setActiveMenu('ufhs');
		return $this;
	}

	private function _outputLayout($title)
	{
		$this->_initAction();
		$this->_title($this->__($title));
		$this->renderLayout();
	}

	public function settypeAction()
	{
		$this->_outputLayout('Resource Types');
	}

	public function newtypeAction()
	{
		$this->_outputLayout('New Resource Types');
	}

	public function viewresourcesAction()
	{
		$this->_outputLayout('Resources');
	}

	public function edittypeAction()
	{
		$this->_outputLayout('Edit Resource Type');
	}

	/**
	 * Add Resource Functions
	 * ----------------------
	 * The add resource functions are responsible for adding certain resources,
	 * including the searchable attributes, resources types and the resource and
	 * file upload functionality for the product backend.
	 *
	 * With the exception of the addResource action, due to the file handling functionality,
	 * the majority of the functionality has been refactored into the _addResource
	 * function, which simple takes the index, of the array passed, and the model
	 * name, in which to add the resource, as parameters.
	 */

	private function _addResource($index,$modelName)
	{
		$post = $this->getRequest()->getParams();
		if(strlen($post[$index]) > 0)
		{
			$resource = Mage::getModel('manuallookup/'.$modelName);
			if(isset($post['id']))
			{
				$resource->load($post['id'])->setId($post['id']);
			}
			$resource->setData($index,$post[$index])->save();
		}
		else
		{
			$this->_throwError('Please enter a valid string.');
		}
	}

	public function addtypeAction()
	{
		$this->_addResource('type','resourcestype');
		$this->_redirect('*/*/settype/');
	}

	public function addresourceAction()
	{
		$iniGet = ini_get('upload_max_filesize');
		$last = strtolower(substr($iniGet, - 1));
		$max = intVal($iniGet);
		switch ($last) {
			case 'g':
			$max = $max * 1024 * 1024 * 1024; break;
			case 'm':
			$max = $max * 1024 * 1024; break;
			case 'k':
			$max = $max * 1024; break;
		}

		$post = $this->getRequest()->getParams();
		$resourceArray = $post['resource'];
		if(!empty($resourceArray) && strlen($resourceArray['name']) > 0 && strlen($resourceArray['type']) > 0) {
			$store = isset($resourceArray['store']) ? $resourceArray['store'] : 0;
			if ($_FILES['resourceFile']['size'] <= $max) {
				if ($_FILES['resourceFile']['error'] == 0) {
					$uploader = new Varien_File_Uploader('resourceFile');
					if($uploader->setAllowRenameFiles(true)->save(Mage::getBaseDir('media') . '/ufhs/manuals/')) {
						Mage::getModel('manuallookup/resources')
						->setProdId($post['prodid'])
						->setResourceTypeId($resourceArray['type'])
						->setFilename($uploader->getUploadedFileName())
						->setName($resourceArray['name'])
						->setStore($store)
						->save();
					} else {
						$this->_throwError('Error occurred saving that file.');
					}
				} else{
					$this->_throwError('Error occurred uploading that file.');
				}
			} else {
				$this->_throwError('That file exceeds the ' . $iniGet . ' filesize limit imposed by the server.');
			}
		} else {
			$this->_throwError('Some values seem to be missing.');
		}
		$this->_redirect('*/catalog_product/edit/', array('active_tab' => 'Resources','id' => $post['prodid']));
	}

	public function addvideoAction()
	{
		$post = $this->getRequest()->getParams();
		$postArray = $post['video'];
		if(!empty($postArray) && strlen($postArray['url']) > 0)
		{
			$exist = Mage::getModel('manuallookup/videos')
			->getCollection()
			->addFieldToFilter('prod_id', $post['prodid'])
			->addFieldToFilter('url', $postArray['url'])
			->getSize() > 0 ? true : false;

			if (!$exist) {
				if (Ufhs_Manuallookup_Helper_Data::checkVideo($postArray['url'])) {
					$name = strlen($postArray['name']) > 0 ? $postArray['name'] : Ufhs_Manuallookup_Helper_Data::getVideoTitle($postArray['url']);
					$store = isset($postArray['store']) ? $postArray['store'] : 0;
					Mage::getModel('manuallookup/videos')
					->setProdId($post['prodid'])
					->setUrl($postArray['url'])
					->setName($name)
					->setStore($store)
					->save();
				} else {
					$this->_throwError("The YouTube ID you supplied doesn't match any videos on YouTube. Please check your code and try again.");
				}
			} else {
				$this->_throwError("That video has already been added to this product.");
			}
		}
		else
		{
			$this->_throwError('Some values seem to be missing.');
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $post['prodid']));
	}

	/**
	 * Delete Resource Functions
	 * -------------------------
	 * The delete resource functions are responsible for deleting certain resources,
	 * including the resource types, searchable attributes and the resources in
	 * the product backend.
	 *
	 * As the functionality and implications of each resource type differ too much,
	 * it has been impossible to refactor these functions, however the nullification
	 * functions, responsible for setting all foriegn keys back to 0, have been
	 * refactored into the _nullifyResource function.
	 */

	public function deleteresourceAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		$prodId = 0;
		if(isset($post['id']))
		{
			$resource = Mage::getModel('manuallookup/resources')->load($post['id']);
			if(isset($resource->getData()['prod_id']))
			{
				$prodId = $resource->getData()['prod_id'];
				$resource->delete();
			}
			else
			{
				$this->_throwError('There seems to be no product assigned to that resource.');
			}
		}
		else
		{
			$this->_throwError('Something has gone horribly wrong...Be afraid, be very afraid!');
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $prodId));
	}

	public function deletevideoAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		$prodId = 0;
		if(isset($post['id']))
		{
			$resource = Mage::getModel('manuallookup/videos')->load($post['id']);
			if(isset($resource->getData()['prod_id']))
			{
				$prodId = $resource->getData()['prod_id'];
				$resource->delete();
			}
			else
			{
				$this->_throwError('There seems to be no product assigned to that resource.');
			}
		}
		else
		{
			$this->_throwError('Something has gone horribly wrong...Be afraid, be very afraid!');
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $prodId));
	}

	public function deletetypeAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if(isset($post['id']))
		{
			$this->_nullifyResource('resource_type_id',$post['id'],'resources');
			$attribute = Mage::getModel('manuallookup/resourcestype')->load($post['id'])->delete();
		}
		$this->_redirect('*/*/settype/');
	}

	private function _nullifyResource($type,$value,$modelName)
	{
		$collection = Mage::getModel('manuallookup/'.$modelName)->getCollection()->addFieldToFilter($type,$value);
		foreach($collection->getData() as $item)
		{
			Mage::getModel('manuallookup/'.$modelName)->load($item['id'])->setData($type,0)->save();
		}
	}

	public function linkresourceAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		$resourceArray = $post['resource'];

		if(!empty($resourceArray) && strlen($resourceArray['id']) > 0 && strlen($post['prodid']) > 0) {
			$oldResource = Mage::getModel('manuallookup/resources')->load($resourceArray['id']);
			$store = isset($resourceArray['store']) ? $resourceArray['store'] : 0;
			Mage::getModel('manuallookup/resources')
			->setProdId($post['prodid'])
			->setResourceTypeId($oldResource->getResourceTypeId())
			->setFilename($oldResource->getFilename())
			->setName($oldResource->getName())
			->setStore($store)
			->save();
		}
		else
		{
			$this->_throwError('Some values seem to be missing.');
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $post['prodid']));
	}

	public function massvideoenableAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['ids'])) {
			foreach ($post['ids'] as $id) {
				Mage::getModel('manuallookup/videos')->load($id)->setDisabled(0)->save();
			}
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $post['prodid']));
	}

	public function massvideodisableAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['ids'])) {
			foreach ($post['ids'] as $id) {
				Mage::getModel('manuallookup/videos')->load($id)->setDisabled(1)->save();
			}
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $post['prodid']));
	}

	public function massvideodeleteAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['ids'])) {
			foreach ($post['ids'] as $id) {
				Mage::getModel('manuallookup/videos')->load($id)->delete();
			}
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $post['prodid']));
	}

	public function massresourcedeleteAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['ids'])) {
			foreach ($post['ids'] as $id) {
				$resource = Mage::getModel('manuallookup/resources')->load($id);
				$filename = $resource->getFilename();
				$resource->delete();
				$count = count(Mage::getModel('manuallookup/resources')->getCollection()->addFieldToFilter('filename', $filename)->getData());
				if ($count <= 0) {
					unlink(Mage::getBaseDir('media') . '/ufhs/manuals/' . $filename);
				}
			}
		}
		$this->_redirect('*/catalog_product/edit/',array('active_tab' => 'Resources','id' => $post['prodid']));
	}


	/**
	 * Error Handling Functions
	 * ------------------------
	 * The throw error function just throws a standard Magento session error with
	 * whatever it is given in the parameters.
	 */

	private function _throwError($error)
	{
		Mage::getSingleton('core/session')->addError($error);
	}

}
<?php

class Ufhs_Manuallookup_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();

		// Update the layout to inject system selector shizzle
		$this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
		$this->getLayout()->getBlock('head')->addCss('manuallookup/css/style.css');

		// Add the system selector template to the content section of the main layout
		$searchBlock = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'manual-lookup', ['template' => 'manuallookup/resource-search.phtml']);
		$this->getLayout()->getBlock('content')->append($searchBlock);

		$blockBrand = $this->getLayout()->createBlock('Ufhs_Manuallookup_Block_Resourcelist', 'resource.list.brand', ['template' => 'manuallookup/list/brand.phtml']);
		$blockCat = $this->getLayout()->createBlock('Ufhs_Manuallookup_Block_Resourcelist', 'resource.list.cat', ['template' => 'manuallookup/list/cat.phtml']);

		$searchBlock->setChild('brand', $blockBrand);
		$searchBlock->setChild('cat', $blockCat);

		$this->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
	}
}